<?php

namespace Database\Factories;

use App\Models\Item;
use App\Models\Stock;
use Illuminate\Database\Eloquent\Factories\Factory;

class StockFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Stock::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'item_id' => Item::all()->random()->id,
            'type' => collect(['in', 'out'])->random(),
            'total' => $this->faker->randomNumber([2])
        ];
    }
}
