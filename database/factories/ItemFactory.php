<?php

namespace Database\Factories;

use App\Models\Item;
use App\Models\Stock;
use Closure;
use Illuminate\Database\Eloquent\Factories\Factory;

class ItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Item::class;

    public function configure()
    {
        return $this->afterCreating(function (Item $item) {
            Stock::factory()->count(5)->create([
                'item_id' => $item->id
            ]);
        });

    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'code' => $this->faker->postcode(),
            'name' => $this->faker->domainName(),
            'image' => $this->faker->image('storage/app/public/images')
        ];
    }
}
