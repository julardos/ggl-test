GGL Life Test Developer
--
I am sorry i can't finish this until can CRUD but i have provide this app with migration, factory, and seeder
to create a dummy data. Thank You ...

#### How to run
> These tutorial below is only for local test.

1. Run following these commands.
    ```shell script
    cp .env.example .env
   ```
   ```shell script
    composer install
   ```

   ```shell script
    php artisan key:generate
    ```
2. Setup database, change db config in `.env`
3. Run migration and seeder
    ```shell script
    php artisan migrate --seed
    ```
4. Run this command below in different terminal session
    ```shell script
    php artisan serve 
    ```
> Made with ♥ 2021 
