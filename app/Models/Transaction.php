<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = ['amount', 'type'];

    public function item() {
        return $this->belongsTo(Item::class, 'item_id', 'id');
    }
}
